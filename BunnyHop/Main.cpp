#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <string>
#include <vector>
#include <cstdlib>
#include <time.h>
#include "Game.h"
#include <stdlib.h>

int main()
{
	// declare our SFML window, called gameWindow
	sf::RenderWindow gameWindow;
	// set up the SFML window, passing the dimmensions and the window name
	gameWindow.create(sf::VideoMode::getDesktopMode(), "Bunny Hop", sf::Style::Titlebar | sf::Style::Close);

    
    //
    //GAME SETUP
    //


	Game gameInstance;
	//this will not end until the game is over
	gameInstance.RunGameLoop();
	//if we get here, the loop exited, so end the program by returning
	return 0;
    
    //game clock
    //Create a clock to track time passed between frames
    sf::Clock gameClock;
    
    //seed random number generator
    srand(time(NULL));

    // Game Music
    sf::Music gameMusic;
    // load our audio using a file path
    gameMusic.openFromFile("Assets/Audio/music.ogg");//alter path
    // start the music
    gameMusic.play();

    // Game Font
    sf::Font gameFont;
    // load our font using a file path
    gameFont.loadFromFile("Assets/Font/mainFont.ttf");//alter path
    
    //Set game over variable
    bool gameOver = false;

    

    //Game Loop
    
	while (gameWindow.isOpen())
	{
        //
        //INPUT SECTION
        //
        

        
        //
        //UPDATE SECTION
        //
        

        
        //
        //DRAW SECTION
        //
        

	}

	return 0;
}