#include "Game.h"

Game::Game()
:window(sf::VideoMode::getDesktopMode(), "Bunny Hop", sf::Style::Titlebar | sf::Style::Close), gameClock()
, levelScreenInstance(this)
{
	
	
	window.setMouseCursorVisible(false);
}

void Game::RunGameLoop()
{
	//for as long as the window is open repeat
	while (window.isOpen())
	{
		Input();
		Update();
		Draw();
	}
}

void Game::Input()
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			window.close();
		}
		//close game if escape is pressed
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
		{
			window.close();
		}
	}
	levelScreenInstance.Input();
}

void Game::Update()
{
	sf::Time frameTime = gameClock.restart();
	levelScreenInstance.Update(frameTime);
}

void Game::Draw()
{
	window.clear();
	levelScreenInstance.DrawTo(window);
	window.display();
}

sf::RenderWindow & Game::GetWindow()
{
	return window;
}
