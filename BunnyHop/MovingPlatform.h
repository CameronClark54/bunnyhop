#pragma once
#include "Platform.h"
class MovingPlatform :
	public Platform
{
	public:
	//constructor
	MovingPlatform(float newMinX, float newMaxX);;

	void Update(sf::Time frameTime);

private:
	float speed;
	float minX;
	float maxX;
};

