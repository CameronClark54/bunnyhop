#pragma once
#include <SFML/Graphics.hpp>
#include "Level.h"
class Game
{
public:
	Game();
	void RunGameLoop();
	void Input();
	void Update();
	void Draw();

	//Getters
	sf::RenderWindow& GetWindow();

private:
	sf::RenderWindow window;
	sf::Clock gameClock;
	LevelScreen levelScreenInstance;
};
