#pragma once
#include "AnimatingObject.h"
class Player :
	public AnimatingObject
{
	public:
	//constructor
	Player(sf::Vector2u screenSize);

	//Functions
	void Input();
	void Update(sf::Time frameTime);
	void HandleSolidCollision(sf::FloatRect otherHitbox);

	private:
	
	sf::Vector2f velocity;
	float speed;
	float gravity;
	sf::Vector2f previousPosition;
};

