#include "Player.h"
#include "AssetManager.h"

Player::Player(sf::Vector2u screenSize)
	: AnimatingObject(AssetManager::RequestTexture("Assets/Graphics/PlayerAnimation.png"), 75, 100, 8.0f)
	, velocity(0.0f, 0.0f)
	, speed(300.0f)
	, gravity(800.0f)
	,previousPosition()
{
	AddClip("Jump", 0, 1);
	

	// position the player at the centre of the screen
	sf::Vector2f newPosition;
	newPosition.x = ((float)screenSize.x - sprite.getGlobalBounds().width) / 2.0f;
	newPosition.y = ((float)screenSize.y - sprite.getGlobalBounds().height) / 2.0f;
	sprite.setPosition(newPosition);
}

void Player::Input()
{
	velocity.x = 0.0f;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		// Move player left
		velocity.x = -speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		// Move player right
		velocity.x = speed;
	}
}

void Player::Update(sf::Time frameTime)
{
	//sets previous position
	previousPosition = sprite.getPosition();

	//calculate new position
	sf::Vector2f newPosition = sprite.getPosition() + velocity * frameTime.asSeconds();

	//move player to the new position
	sprite.setPosition(newPosition);
	AnimatingObject::Update(frameTime);

	//calculate new velocity
	velocity.y = velocity.y + gravity * frameTime.asSeconds();
}

void Player::HandleSolidCollision(sf::FloatRect otherHitbox)
{
	//if player collides with platform
	if (GetHitbox().intersects(otherHitbox)) 
	{
		//if we hit the platform from above
		float previousBottom = previousPosition.y + GetHitbox().height;
		float platformTop = otherHitbox.top;
		if (previousBottom < platformTop)
		{
			//set the upward velocity to a jump value.
			const float JUMP_VALUE = -600; // negative to go up
			velocity.y = JUMP_VALUE;
			PlayClip("Jump", false);
		}
	}
}
