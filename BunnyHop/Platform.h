#pragma once
#include "SpriteObject.h"
class Platform :
	public SpriteObject
{
public:
	// Constructors
	Platform();
	Platform(sf::Texture& newTexture);
	void SetPosition(sf::Vector2f newPosition);
	virtual void Update(sf::Time frameTime);
};

