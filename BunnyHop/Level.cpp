#include "Level.h"
#include "Game.h"
#include "MovingPlatform.h"
#include <stdlib.h>


LevelScreen::LevelScreen(Game* newGamePointer)
	:playerInstance(newGamePointer->GetWindow().getSize())
	, gamePointer(newGamePointer)
	,platforms()
	,camera(newGamePointer->GetWindow().getDefaultView())
	, platformGap(50)
	, platformGapIncrease(3)
	, highestPlatform(0)
	, platformBuffer(100)
{
	//create a staring platform
	Platform* platformInstance= new Platform();

	//calculate center of the screen
	sf::Vector2f newPosition;
	newPosition.x = newGamePointer->GetWindow().getSize().x / 2;
	newPosition.y = newGamePointer->GetWindow().getSize().y / 2;

	//calculate position of platform to be centered
	newPosition.x -= platformInstance->GetHitbox().width / 2;
	newPosition.y -= platformInstance->GetHitbox().height / 2;

	//add to the y position to lower the platform
	const float PLATFORM_OFFSET_1 = 150; // can be adjusted as needed
	newPosition.y += PLATFORM_OFFSET_1;

	//set the new position of the platform
	platformInstance->SetPosition(newPosition);

	//set the new position of the platform
	platformInstance->SetPosition(newPosition);

	//copy the starting platform into a vector
	platforms.push_back(platformInstance);


	//populate the rest of the platforms
	highestPlatform = newPosition.y;

	//place platforms until the highest one is out of the cameras view
	float cameraTop = camera.getCenter().y - camera.getSize().x / 2.0f;

	while (highestPlatform > cameraTop - platformBuffer) 
	{
		AddPlatform();
	}
}

void LevelScreen::Input()
{
	playerInstance.Input();
}

void LevelScreen::Update(sf::Time frameTime)
{
	playerInstance.Update(frameTime);
	for (int i = 0; i < platforms.size(); ++i)
	{
		playerInstance.HandleSolidCollision(platforms[i]->GetHitbox());
	}

	// place a new platform if needed
	float cameraTop = camera.getCenter().y - camera.getSize().x / 2.0f;
	if (highestPlatform > cameraTop - platformBuffer)
	{
		AddPlatform();
	}

	// If the bottom platform is off screen, remove it
	if (platforms[0]->GetHitbox().top > camera.getCenter().y + camera.getSize().y / 2) 
	{
		delete platforms[0]; // delete the actual platform (freeing memory)
		platforms.erase(platforms.begin()); // erase the first element of the vector (an address is removed)
	}

	//update platforms
	for (int i = 0; i < platforms.size(); ++i)
	{
		platforms[i]->Update(frameTime);
	}

}

void LevelScreen::DrawTo(sf::RenderTarget & target)
{
	//update camera position
	sf::Vector2f currentViewCenter = camera.getCenter();
	float playerCenterY = playerInstance.GetHitbox().top + playerInstance.GetHitbox().height / 2;
	if (playerCenterY < currentViewCenter.y)
	{
		camera.setCenter(currentViewCenter.x, playerCenterY);
	}

	//set camera view
	target.setView(camera);

	//draw things on screen
	playerInstance.DrawTo(target);

	for (int i = 0; i < platforms.size(); ++i)
	{
		platforms[i]->DrawTo(target);
	}


	//remove camera view
	target.setView(target.getDefaultView());
}

void LevelScreen::AddPlatform()
{
	//calculates the screen area
	int PLAY_AREA_WIDTH = 800;
	int minX = camera.getCenter().x - PLAY_AREA_WIDTH / 2;
	int maxX = camera.getCenter().x + PLAY_AREA_WIDTH / 2;

	//creates a new platform
	Platform* newPlatform = nullptr;

	//chooses the type of platform to create
	int choiceMin = 0;
	int choiceMax = 100;
	int choice = rand() % (choiceMax - choiceMin) + choiceMin;
	int chanceMoving = 50;
	if (choice < 50)
	{
		newPlatform = new MovingPlatform(minX, maxX);
	}
	else
	{
		newPlatform = new Platform();
	}

	sf::Vector2f newPosition;
	newPosition.y = highestPlatform - platformGap;


	//randomise the platforms X position
	maxX -= newPlatform->GetHitbox().width;
	newPosition.x = rand() % (maxX - minX) + minX;

	//set the new platforms position
	newPlatform->SetPosition(newPosition);

	//add the new platform to the vector
	platforms.push_back(newPlatform);

	//update platform gap
	platformGap += platformGapIncrease;

	//update current highest platform
	highestPlatform = newPosition.y;
}
