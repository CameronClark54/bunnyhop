#include "MovingPlatform.h"
#include "AssetManager.h"


MovingPlatform::MovingPlatform(float newMinX, float newMaxX)
:Platform(AssetManager::RequestTexture("Assets/Graphics/MovingPlatform.png"))
, speed(100)
, minX(newMinX)
, maxX(newMaxX)
{

}

void MovingPlatform::Update(sf::Time frameTime)
{
	//record original position
	sf::Vector2f originalPosition = sprite.getPosition();

	//calculate the new position
	sf::Vector2f newPosition = sprite.getPosition();
	newPosition.x += speed * frameTime.asSeconds();
	//move the platform to the new position
	sprite.setPosition(newPosition);

	//check if we are off screen to the right or left
	int platformLeft = GetHitbox().left;
	int platformRight = GetHitbox().left + GetHitbox().width;
	if (platformLeft < minX || platformRight > maxX) 
	{
		//turn around
		speed *= -1.0f;

		//return to the position we were at before we went out of bounds
		sprite.setPosition(originalPosition);
	}
}

